package controller;

import javax.swing.JOptionPane;

public  class AnalizadorEstandar {
	public static String _ingreso;
	
	public AnalizadorEstandar(String ingreso) 
	{
		_ingreso = ingreso;
		analizarCadena(_ingreso);
		//realizarOperaciones();
	}

	private void analizarCadena(String cadena) 
	{
		verificarSintaxis(cadena);
		int terminos = cantidadTerminos(cadena);
		System.out.println(terminos);
	}

	private void verificarSintaxis(String cadena) {
		if(cadena.isEmpty()) 
		{
			JOptionPane.showMessageDialog(null, "Debe ingresar una expresion.");
		}
		if(!analizerSintaxis(cadena))
		{
			JOptionPane.showMessageDialog(null, "Verifique la sintaxis ingresada.");
		}
		
	}
	private boolean analizerSintaxis(String cadena) {
		//Verifico que el primer caracter no sea un signo no valido
		if(cadena.charAt(0)=='x' || cadena.charAt(0)=='/')
			return false;
		int largo= cadena.length();
		
		// Verifico que no se finalice con un signo
		if(esSigno(cadena.charAt(largo-1))) 
		{
			JOptionPane.showMessageDialog(null, "La expresion ingresada no puede finalizar con algun signo.");
			return false;
		}
		if(verificoSignosConsecutivos(cadena)) {
			JOptionPane.showMessageDialog(null, "Error de Sintaxis, no pueden estar dos o más signos juntos.");
		}
		return true;
	}

	private boolean verificoSignosConsecutivos(String cadena) {
		boolean ret = false;
		int contador=0;
		for(char c: cadena.toCharArray()) {
			if(esSigno(c)) {
				contador ++;
				if(contador >= 2) {
					ret = true;
					break;
				}
			}else {
				contador=0;
			}
		}
		return ret;
	}

	private boolean esSigno(char c)
	{
		boolean ret = false;
		String signos = "+-x/";
		for (char letra : signos.toCharArray()) {
			if(letra== c) {
				ret = true;
			}
		}
		return ret;
	}

	private int cantidadTerminos(String cadena) {
		int cantidad =1;
		for (char letra :cadena.toCharArray()){
			if(letra=='+' || letra=='-') {
				cantidad ++;
			}
		}
		 return cantidad;
	}

}
