package calculadora;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.UIManager;

import controller.AnalizadorEstandar;

import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Font;

public class Calculadora {

	private JFrame frmCalculadora;
	private JTextField panel;
	static JButton btn0;
	static JButton btn1;
	static JButton btn2;
	static JButton btn3;
	static JButton btn4;
	static JButton btn5;
	static JButton btn6;
	static JButton btn7;
	static JButton btn8;
	static JButton btn9;
	static JButton btnSumar;
	static JButton btnRestar;
	static JButton btnMultiplicar;
	static JButton btnDividir;
	static JButton btnRet;
	static JButton btnClear;
	static AnalizadorEstandar analizer;
	static ArrayList<String> ingresos= new ArrayList<String>();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculadora window = new Calculadora();
					window.frmCalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Calculadora() {
		initialize();
		try{
			 
			  JFrame.setDefaultLookAndFeelDecorated(true);
			 // JDialog.setDefaultLookAndFeelDecorated(true);
			  
			  //UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			  UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
			  //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			  //UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
			}
			catch (Exception e)
			 {
			  e.printStackTrace();
			 }
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculadora = new JFrame();
		frmCalculadora.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		frmCalculadora.setResizable(false);
		frmCalculadora.getContentPane().setBackground(SystemColor.activeCaption);
		frmCalculadora.setTitle("Calculadora");
		frmCalculadora.setBounds(100, 100, 354, 325);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCalculadora.getContentPane().setLayout(null);
		
		panel = new JTextField();
		panel.setFont(new Font("Verdana", Font.BOLD, 15));
		panel.setBackground(Color.WHITE);
		panel.setEditable(false);
		panel.setBounds(10, 11, 318, 107);
		frmCalculadora.getContentPane().add(panel);
		panel.setColumns(10);
		
		btn1 = new JButton("1");
		presionarBtn(btn1);
		btn1.setBounds(10, 149, 46, 23);
		frmCalculadora.getContentPane().add(btn1);
		
		btn2 = new JButton("2");
		presionarBtn(btn2);
		btn2.setBounds(66, 149, 46, 23);
		frmCalculadora.getContentPane().add(btn2);
		
		btn3 = new JButton("3");
		presionarBtn(btn3);
		btn3.setBounds(122, 149, 46, 23);
		frmCalculadora.getContentPane().add(btn3);
		
		btn4 = new JButton("4");
		presionarBtn(btn4);
		btn4.setBounds(10, 183, 46, 23);
		frmCalculadora.getContentPane().add(btn4);
		
		btn5 = new JButton("5");
		presionarBtn(btn5);
		btn5.setBounds(66, 183, 46, 23);
		frmCalculadora.getContentPane().add(btn5);
		
		btn6 = new JButton("6");
		presionarBtn(btn6);
		btn6.setBounds(122, 183, 46, 23);
		frmCalculadora.getContentPane().add(btn6);
		
		btn7 = new JButton("7");
		presionarBtn(btn7);
		btn7.setBounds(10, 217, 46, 23);
		frmCalculadora.getContentPane().add(btn7);
		
		btn8 = new JButton("8");
		presionarBtn(btn8);
		btn8.setBounds(66, 217, 46, 23);
		frmCalculadora.getContentPane().add(btn8);
		
		btn9 = new JButton("9");
		presionarBtn(btn9);
		btn9.setBounds(122, 217, 46, 23);
		frmCalculadora.getContentPane().add(btn9);
		
		btn0 = new JButton("0");
		presionarBtn(btn0);
		btn0.setBounds(66, 251, 46, 23);
		frmCalculadora.getContentPane().add(btn0);
		
		btnSumar = new JButton("+");
		presionarBtn(btnSumar);
		btnSumar.setBounds(207, 149, 46, 23);
		frmCalculadora.getContentPane().add(btnSumar);
		
		btnRestar = new JButton("-");
		presionarBtn(btnRestar);
		btnRestar.setBounds(207, 183, 46, 23);
		frmCalculadora.getContentPane().add(btnRestar);
		
		btnMultiplicar = new JButton("x");
		presionarBtn(btnMultiplicar);
		btnMultiplicar.setBounds(207, 217, 46, 23);
		frmCalculadora.getContentPane().add(btnMultiplicar);
		
		btnDividir = new JButton("/");
		presionarBtn(btnDividir);
		btnDividir.setBounds(207, 251, 46, 23);
		frmCalculadora.getContentPane().add(btnDividir);
		
		btnClear = new JButton("CC");
		btnClear.setFont(new Font("Tahoma", Font.BOLD, 8));
		limpiarPantalla(btnClear);
		btnClear.setBounds(282, 149, 46, 23);
		frmCalculadora.getContentPane().add(btnClear);
		
		btnRet = new JButton("=");
		btnRet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				analizer = new AnalizadorEstandar(panel.getText());
				System.out.println(ingresos.toString());
			}
		});
		//presionarBtn(btnRet);
		btnRet.setBounds(282, 183, 46, 23);
		frmCalculadora.getContentPane().add(btnRet);
	}
	
	private void limpiarPantalla(JButton btn) {
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setText("");
				ingresos.clear();
			}
		});
		
	}

	private void presionarBtn(final JButton btn)
	{
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				escribirPanel(btn);
				ingresos.add(btn.getText());
			}
		});
	}
	
	private void escribirPanel(JButton btn)
	{
		String contenido = panel.getText().concat(btn.getText());
		panel.setText(contenido);
	}
}
